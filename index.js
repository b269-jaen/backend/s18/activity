/* 1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function


	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

// 1.
function sumOfNumbers(num1, num2){
	let sumThis = num1 + num2;
	console.log("Displayed sum of " + num1 +" and " +num2);
	console.log(sumThis);
};
sumOfNumbers(6, 2);

function differenceOfNumbers(num1, num2){
	let subThis = num1 - num2;
	console.log("Displayed difference of " + num1 +" and " +num2);
	console.log(subThis);
};
differenceOfNumbers(6, 2);



// 2.
function productOfNumbers(num1, num2){
	let multiplyThis = num1*num2;
	console.log("The product of "+ num1 + " and " +num2)
	return multiplyThis;	
};
let product = (productOfNumbers(6,2));
console.log(product);

function quotientOfNumbers(num1, num2){
	let divideThis = num1 / num2;
	console.log("The quotient of "+ num1 + " and " +num2)
	return divideThis;
}
let quotient = quotientOfNumbers(6,2);
console.log(quotient);



// 3.
function areaOfCircle(radius){
	let area =  Math.PI * radius**2;
	console.log("The result of getting the are of a circle with " + radius + " radius")
	return area;
}
let circleArea = areaOfCircle(6);
console.log(circleArea);



// 4.
function averageOfNumbers(num1, num2, num3, num4){
	average = (num1 + num2 + num3 + num4) / arguments.length;
	console.log("The average of " + num1 + ", " +num2 + ", " +num3 + ", " + num4)
	return average;
};
let averageVar = averageOfNumbers(1 , 2, 3, 4);
console.log(averageVar);

// 5.
function passOrFailed(score, totalScore){
	let percentage = (score / totalScore) * 100;
	let isPassed =  percentage >= 75;
	console.log("Is " + score + "/" + totalScore + " a passing score?");
	return isPassed;
}
let isPassingScore = passOrFailed(32, 50);
console.log(isPassingScore);


